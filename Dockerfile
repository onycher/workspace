﻿FROM ubuntu

WORKDIR /root

ENV TERM xterm-256color
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install -y build-essential
RUN apt-get install -y libtool libtool-bin autoconf automake cmake pkg-config unzip
RUN apt-get install -y autoconf blt blt-dev debhelper dh-apparmor docutils-common libbluetooth-dev libbz2-dev libdb-dev libdb5.3-dev libfontconfig1-dev libfreetype6-dev libgdbm-dev libice-dev libjs-jquery libjs-sphinxdoc libjs-underscore liblzma-dev libmpdec-dev libncursesw5-dev libpng12-dev libreadline6-dev libsigsegv2 libsm-dev libsqlite3-dev libtcl8.6 libtinfo-dev libtk8.6 libxext-dev libxft-dev libxrender-dev libxss-dev libxt-dev m4 po-debconf python-docutils python-jinja2 python-markupsafe python-pygments python-roman python-sphinx quilt sharutils sphinx-common tcl tcl-dev tcl8.6 tcl8.6-dev tk tk-dev tk8.6 tk8.6-dev x11proto-render-dev x11proto-scrnsaver-dev libffi-dev libssl-dev
RUN apt-get install -y zsh tmux wget
RUN apt-get install -y apt-utils
RUN apt-get install -y net-tools
RUN apt-get install -y lsof
RUN apt-get install -y smbclient
RUN apt-get install -y dnsutils
RUN apt-get install -y dsniff
RUN apt-get install -y host
RUN apt-get install -y netcat
RUN apt-get install -y tcpdump
RUN apt-get install -y tcpreplay
RUN apt-get install -y nmap
RUN apt-get install -y wireless-tools
RUN apt-get install -y python-pip
RUN pip install --upgrade pip

RUN pip install jedi

RUN pip install pipenv

RUN pip install neovim

RUN apt-get install -y git
RUN git config --global user.name "Sebastian Onycher"
RUN git config --global user.email onycher@gmail.com


RUN git clone https://github.com/python/cpython
RUN cd cpython && git checkout 3.6
RUN cd cpython && ./configure --enable-optimizations && make && make install
RUN rm -rf cpython

RUN pip3 install pipenv
RUN pip3 install jedi
RUN pip3 install neovim

RUN git clone https://github.com/neovim/neovim
RUN cd neovim && make && make install
RUN rm -rf neovim

RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true
RUN sed -i -E "s/^ZSH_THEME=\".*\"$/ZSH_THEME=\"af-magic\"/" ~/.zshrc

RUN mkdir -p .config/nvim/plugins
RUN curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
RUN sh ./installer.sh .config/nvim/dein
RUN rm installer.sh

RUN git clone https://github.com/connrs/dotfiles
RUN cat dotfiles/tmux/base16.tmux >> .tmux.conf
RUN rm -rf dotfiles

RUN echo "export LS_COLORS='no=00;38;5;244:rs=0:di=00;38;5;33:ln=00;38;5;37:mh=00:pi=48;5;230;38;5;136;01:so=48;5;230;38;5;136;01:do=48;5;230;38;5;136;01:bd=48;5;230;38;5;244;01:cd=48;5;230;38;5;244;01:or=48;5;235;38;5;160:su=48;5;160;38;5;230:sg=48;5;136;38;5;230:ca=30;41:tw=48;5;64;38;5;230:ow=48;5;235;38;5;33:st=48;5;33;38;5;230:ex=00;38;5;64:*.tar=00;38;5;61:*.tgz=00;38;5;61:*.arj=00;38;5;61:*.taz=00;38;5;61:*.lzh=00;38;5;61:*.lzma=00;38;5;61:*.tlz=00;38;5;61:*.txz=00;38;5;61:*.zip=00;38;5;61:*.z=00;38;5;61:*.Z=00;38;5;61:*.dz=00;38;5;61:*.gz=00;38;5;61:*.lz=00;38;5;61:*.xz=00;38;5;61:*.bz2=00;38;5;61:*.bz=00;38;5;61:*.tbz=00;38;5;61:*.tbz2=00;38;5;61:*.tz=00;38;5;61:*.deb=00;38;5;61:*.rpm=00;38;5;61:*.jar=00;38;5;61:*.rar=00;38;5;61:*.ace=00;38;5;61:*.zoo=00;38;5;61:*.cpio=00;38;5;61:*.7z=00;38;5;61:*.rz=00;38;5;61:*.apk=00;38;5;61:*.gem=00;38;5;61:*.jpg=00;38;5;136:*.JPG=00;38;5;136:*.jpeg=00;38;5;136:*.gif=00;38;5;136:*.bmp=00;38;5;136:*.pbm=00;38;5;136:*.pgm=00;38;5;136:*.ppm=00;38;5;136:*.tga=00;38;5;136:*.xbm=00;38;5;136:*.xpm=00;38;5;136:*.tif=00;38;5;136:*.tiff=00;38;5;136:*.png=00;38;5;136:*.PNG=00;38;5;136:*.svg=00;38;5;136:*.svgz=00;38;5;136:*.mng=00;38;5;136:*.pcx=00;38;5;136:*.dl=00;38;5;136:*.xcf=00;38;5;136:*.xwd=00;38;5;136:*.yuv=00;38;5;136:*.cgm=00;38;5;136:*.emf=00;38;5;136:*.eps=00;38;5;136:*.CR2=00;38;5;136:*.ico=00;38;5;136:*.tex=00;38;5;245:*.rdf=00;38;5;245:*.owl=00;38;5;245:*.n3=00;38;5;245:*.ttl=00;38;5;245:*.nt=00;38;5;245:*.torrent=00;38;5;245:*.xml=00;38;5;245:*Makefile=00;38;5;245:*Rakefile=00;38;5;245:*Dockerfile=00;38;5;245:*build.xml=00;38;5;245:*rc=00;38;5;245:*1=00;38;5;245:*.nfo=00;38;5;245:*README=00;38;5;245:*README.txt=00;38;5;245:*readme.txt=00;38;5;245:*.md=00;38;5;245:*README.markdown=00;38;5;245:*.ini=00;38;5;245:*.yml=00;38;5;245:*.cfg=00;38;5;245:*.conf=00;38;5;245:*.h=00;38;5;245:*.hpp=00;38;5;245:*.c=00;38;5;245:*.cpp=00;38;5;245:*.cxx=00;38;5;245:*.cc=00;38;5;245:*.objc=00;38;5;245:*.sqlite=00;38;5;245:*.go=00;38;5;245:*.sql=00;38;5;245:*.csv=00;38;5;245:*.log=00;38;5;240:*.bak=00;38;5;240:*.aux=00;38;5;240:*.lof=00;38;5;240:*.lol=00;38;5;240:*.lot=00;38;5;240:*.out=00;38;5;240:*.toc=00;38;5;240:*.bbl=00;38;5;240:*.blg=00;38;5;240:*~=00;38;5;240:*#=00;38;5;240:*.part=00;38;5;240:*.incomplete=00;38;5;240:*.swp=00;38;5;240:*.tmp=00;38;5;240:*.temp=00;38;5;240:*.o=00;38;5;240:*.pyc=00;38;5;240:*.class=00;38;5;240:*.cache=00;38;5;240:*.aac=00;38;5;166:*.au=00;38;5;166:*.flac=00;38;5;166:*.mid=00;38;5;166:*.midi=00;38;5;166:*.mka=00;38;5;166:*.mp3=00;38;5;166:*.mpc=00;38;5;166:*.ogg=00;38;5;166:*.opus=00;38;5;166:*.ra=00;38;5;166:*.wav=00;38;5;166:*.m4a=00;38;5;166:*.axa=00;38;5;166:*.oga=00;38;5;166:*.spx=00;38;5;166:*.xspf=00;38;5;166:*.mov=00;38;5;166:*.MOV=00;38;5;166:*.mpg=00;38;5;166:*.mpeg=00;38;5;166:*.m2v=00;38;5;166:*.mkv=00;38;5;166:*.ogm=00;38;5;166:*.mp4=00;38;5;166:*.m4v=00;38;5;166:*.mp4v=00;38;5;166:*.vob=00;38;5;166:*.qt=00;38;5;166:*.nuv=00;38;5;166:*.wmv=00;38;5;166:*.asf=00;38;5;166:*.rm=00;38;5;166:*.rmvb=00;38;5;166:*.flc=00;38;5;166:*.avi=00;38;5;166:*.fli=00;38;5;166:*.flv=00;38;5;166:*.gl=00;38;5;166:*.m2ts=00;38;5;166:*.divx=00;38;5;166:*.webm=00;38;5;166:*.axv=00;38;5;166:*.anx=00;38;5;166:*.ogv=00;38;5;166:*.ogx=00;38;5;166:'; " >> .zshrc

RUN echo "set-option -g default-shell /bin/zsh" >> .tmux.conf
RUN echo "set-window-option -g mode-keys vi" >> .tmux.conf
RUN echo "set -sg escape-time 0" >> .tmux.conf
RUN echo "set-option -g status-position top" >> .tmux.conf

RUN git clone https://onycher@bitbucket.org/onycher/workspace.git
RUN mv workspace/init.vim .config/nvim/
RUN cat workspace/tmux.conf >> .tmux.conf
RUN rm -rf workspace

RUN nvim +"call dein#install" +qall
RUN nvim +"UpdateRemotePlugins" +qall

RUN sed -i -E "s/^plugins=\((.*)\)$/plugins=(\1 tmux python virtualenv)/" ~/.zshrc

RUN apt-get install -y  openssh-server
RUN apt-get install -y iputils-ping

WORKDIR /root/workspace
CMD tmux -2

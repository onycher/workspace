"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

set runtimepath+=/root/.config/nvim/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('/root/.config/nvim/dein')
  call dein#begin('/root/.config/nvim/dein')

  call dein#add('/root/.config/nvim/dein/repos/github.com/Shougo/dein.vim')
  call dein#add('Shougo/deoplete.nvim')
  call dein#add('w0rp/ale')
  call dein#add('mhartington/oceanic-next')
  call dein#add('junegunn/fzf', { 'build': './install --all', 'merged': 0 }) 
  call dein#add('junegunn/fzf.vim', { 'depends': 'fzf' })
  call dein#add('vim-airline/vim-airline')
  call dein#add('vim-airline/vim-airline-themes')
  call dein#add('edkolev/tmuxline.vim')
  call dein#add('zchee/deoplete-jedi')
  call dein#end()
  call dein#save_state()
endif

filetype plugin indent on
syntax enable


" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

"End dein Scripts-------------------------

" Enable plugins
let g:deoplete#enable_at_startup=1
autocmd CompleteDone * pclose


set background=dark
if (has("termguicolors"))
 set termguicolors
endif

" Theme
colorscheme OceanicNext
let g:airline_theme='oceanicnext'
set fileencoding=utf-8
set fileencodings=ucs-bom,utf-8,gbk,cp936,latin-1
set fileformat=unix
set fileformats=unix,dos,mac
filetype on
filetype plugin on
filetype plugin indent on
syntax on
set smartindent
set expandtab         "tab to spaces
set tabstop=4         "the width of a tab
set shiftwidth=4      "the width for indent
set foldenable
set foldmethod=indent "folding by indent
set foldlevel=99
set ignorecase        "ignore the case when search texts
set smartcase         "if searching text contains uppercase case will not be ignored
set number           "line number
set cursorline       "hilight the line of the cursor
set cursorcolumn     "hilight the column of the cursor
set nowrap           "no line wrapping
